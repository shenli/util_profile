LOOP_LEN = 30000
SLEEP_LEN = 0.5
UTIL_LEN = 10
EXP_LEN = 30
CMD_CPU_TIME = "head -n 1 /proc/stat"
CMD_KILL_ALL = "kill `ps -ef | grep 'unit.py' | grep -v grep | awk '{print $2}'`"
CMD_NEW_ONE = "python unit.py &> /dev/null &"
