import conf
import commands
import re
import time

def get_cpu_time():
    cpuTimeStruct = commands.getstatusoutput(conf.CMD_CPU_TIME)
    cpuTimeStr = cpuTimeStruct[1]
    cpuTimeStrArr = re.split(r'[ \t\n]+', cpuTimeStr)[1:]
    cpuTimeArr = []
    for item in cpuTimeStrArr:
        cpuTimeArr.append(int(item))
    cpuTimeBusy = sum(cpuTimeArr[0:3])
    cpuTimeAll = sum(cpuTimeArr)
    return (cpuTimeBusy, cpuTimeAll)

def print_util():
    prevBusy, prevAll = get_cpu_time()
    while True:
        time.sleep(conf.UTIL_LEN)
        nowBusy, nowAll = get_cpu_time()
        delBusy = nowBusy - prevBusy
        delAll = nowAll - prevAll
        util = float(delBusy) / delAll
        prevBusy = nowBusy
        prevAll = nowAll
        print util

def main():
    print_util()

if __name__ == '__main__':
    main()
