import util_reader
import conf
import time
import os

for unitCnt in range(20):
    os.popen(conf.CMD_NEW_ONE)
    prevBusy, prevAll = util_reader.get_cpu_time()    
    time.sleep(conf.EXP_LEN)
    nowBusy, nowAll = util_reader.get_cpu_time()
    util = float(nowBusy - prevBusy) / (nowAll - prevAll)
    print (unitCnt + 1, util)

os.popen(conf.CMD_KILL_ALL)
