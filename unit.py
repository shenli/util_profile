import time
import conf
import math

tmp = 1.0
while True:
    for i in range(conf.LOOP_LEN):
        j = i + 1
        tmp += j
        tmp *= j
        tmp -= j
        tmp = math.sqrt(tmp)
        #print tmp
    time.sleep(conf.SLEEP_LEN)

print tmp
